from setuptools import setup, find_packages

setup(
    name='tp1_ex8',
    version='1.0',
    packages=find_packages(exclude=['tests*']),
    author="Thomas SANDIER",
    author_email="thomas.sandier@cpe.fr",
    description="calculatrice pour nombres complexes.",
)