class SimpleComplexCalculator:
    """
    Une calculatrice simple pour les nombres complexes.

    Cette calculatrice fournit des opérations de base pour les nombres complexes, y compris l'addition, la soustraction,
    la multiplication et la division.

    Attributs:
        Aucun

    Méthodes:
        complex_sum: Calcule la somme de deux nombres complexes.
        complex_subtract: Calcule la différence entre deux nombres complexes.
        complex_multiply: Calcule le produit de deux nombres complexes.
        complex_divide: Calcule la division de deux nombres complexes.
    """

    def complex_sum(self, complex1, complex2):
        """
        Calcule la somme de deux nombres complexes.

        Args:
            complex1 (list): Le premier nombre complexe sous la forme [partie_réelle, partie_imaginaire].
            complex2 (list): Le deuxième nombre complexe sous la forme [partie_réelle, partie_imaginaire].

        Returns:
            list: Le résultat de l'addition sous la forme [partie_réelle, partie_imaginaire].
        """
        if not isinstance(complex1[0], int) or not isinstance(complex1[1], int) or not isinstance(complex2[0], int) or not isinstance(complex2[1], int):
            return "ERROR"
        real_part = complex1[0] + complex2[0]  # Calcul de la partie réelle
        imaginary_part = complex1[1] + complex2[1]  # Calcul de la partie imaginaire
        return [
            real_part,
            imaginary_part,
        ]  # Retourne le résultat sous forme d'une liste

    def complex_subtract(self, complex1, complex2):
        """
        Calcule la différence entre deux nombres complexes.

        Args:
            complex1 (list): Le premier nombre complexe sous la forme [partie_réelle, partie_imaginaire].
            complex2 (list): Le deuxième nombre complexe sous la forme [partie_réelle, partie_imaginaire].

        Returns:
            list: Le résultat de la soustraction sous la forme [partie_réelle, partie_imaginaire].
        """
        if not isinstance(complex1[0], int) or not isinstance(complex1[1], int) or not isinstance(complex2[0], int) or not isinstance(complex2[1], int):
            return "ERROR"
        real_part = complex1[0] - complex2[0]  # Calcul de la partie réelle
        imaginary_part = complex1[1] - complex2[1]  # Calcul de la partie imaginaire
        return [
            real_part,
            imaginary_part,
        ]  # Retourne le résultat sous forme d'une liste

    def complex_multiply(self, complex1, complex2):
        """
        Calcule le produit de deux nombres complexes.

        Args:
            complex1 (list): Le premier nombre complexe sous la forme [partie_réelle, partie_imaginaire].
            complex2 (list): Le deuxième nombre complexe sous la forme [partie_réelle, partie_imaginaire].

        Returns:
            list: Le résultat de la multiplication sous la forme [partie_réelle, partie_imaginaire].
        """
        if not isinstance(complex1[0], int) or not isinstance(complex1[1], int) or not isinstance(complex2[0], int) or not isinstance(complex2[1], int):
            return "ERROR"
        real_part = complex1[0] * complex2[0] - complex1[1] * complex2[1]  # Calcul de la partie réelle
        imaginary_part = complex1[0] * complex2[1] + complex1[1] * complex2[0]  # Calcul de la partie imaginaire
        return [
            real_part,
            imaginary_part,
        ]  # Retourne le résultat sous forme d'une liste

    def complex_divide(self, complex1, complex2):
        """
        Calcule la division de deux nombres complexes.

        Args:
            complex1 (list): Le premier nombre complexe sous la forme [partie_réelle, partie_imaginaire].
            complex2 (list): Le deuxième nombre complexe sous la forme [partie_réelle, partie_imaginaire].

        Returns:
            list: Le résultat de la division sous la forme [partie_réelle, partie_imaginaire].
        """
        if not isinstance(complex1[0], int) or not isinstance(complex1[1], int) or not isinstance(complex2[0], int) or not isinstance(complex2[1], int):
            return "ERROR"
        if complex2[0] == 0 and complex2[1] == 0:
            raise ZeroDivisionError("Cannot divide by zero")
        denominator = complex2[0] ** 2 + complex2[1] ** 2  # Calcul du dénominateur
        real_part = (complex1[0] * complex2[0] + complex1[1] * complex2[1]) / denominator  # Calcul de la partie réelle
        imaginary_part = (complex1[1] * complex2[0] - complex1[0] * complex2[1]) / denominator  # Calcul de la partie imaginaire
        return [
            real_part,
            imaginary_part,
        ]  # Retourne le résultat sous forme d'une liste
