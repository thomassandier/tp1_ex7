import unittest
from calculator.calculator import SimpleComplexCalculator

class TestSimpleComplexCalculator(unittest.TestCase):
    def setUp(self):
        self.calculator = SimpleComplexCalculator()

    def test_complex_sum(self):
        complex1 = [4, 5]
        complex2 = [2, 3]
        result = self.calculator.complex_sum(complex1, complex2)
        self.assertEqual(result, [6, 8])

    def test_complex_subtract(self):
        complex1 = [4, 5]
        complex2 = [2, 3]
        result = self.calculator.complex_subtract(complex1, complex2)
        self.assertEqual(result, [2, 2])

    def test_complex_multiply(self):
        complex1 = [4, 5]
        complex2 = [2, 3]
        result = self.calculator.complex_multiply(complex1, complex2)
        self.assertEqual(result, [-7, 22])

    def test_complex_divide(self):
        complex1 = [4, 5]
        complex2 = [2, 3]
        result = self.calculator.complex_divide(complex1, complex2)
        self.assertAlmostEqual(result[0], 1.77, places=2)
        self.assertAlmostEqual(result[1], -0.15, places=2)  # Modified expected value

        # Test de division par zéro
        with self.assertRaises(ZeroDivisionError):
            self.calculator.complex_divide(complex1, [0, 0])

if __name__ == "__main__":
    unittest.main()

    